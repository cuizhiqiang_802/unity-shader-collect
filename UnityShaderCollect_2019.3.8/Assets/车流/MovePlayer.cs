﻿using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.AI;
public class MovePlayer : MonoBehaviour
{
    public Transform hedef;
    NavMeshAgent agent;
    public Roadifier roadifier;
    List<Vector3> showPoints = new List<Vector3>();
    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(hedef.position);
        agent.isStopped = true;


    }

    void Start()
    {
        this.transform.ObserveEveryValueChanged(_ => _.position).Subscribe(_ =>
        {
            CreatRoad();
        });
    }
    private void Update()
    {

    }

    void ClearOldRoad()
    {
        GameObject previousRoad = GameObject.Find("Roadifier Road");
        if (previousRoad)
        {
            Destroy(previousRoad);
        }
    }
    // Update is called once per frame
    void CreatRoad()
    {
        ClearOldRoad();
        var nav = GetComponent<NavMeshAgent>();
        if (nav == null || nav.path == null)
            return;
        var path = nav.path;
        if (nav != null || nav.path != null)
        {
            List<Vector3> points = new List<Vector3>();
            showPoints.Clear();
            for (int i = 0; i < path.corners.Length; i++)
            {
                points.Add(path.corners[i]);
                showPoints.Add(path.corners[i]);
            }
            roadifier.GenerateRoad(points);
        }


    }
}
